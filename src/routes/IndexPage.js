import React, {PureComponent} from 'react';
import { connect } from 'dva';
import {Button} from 'antd';
import styles from './IndexPage.less';


class IndexPage extends PureComponent{
  state = {}
  render() {
    return (
    <div className={styles.normal}>
      <h1 className={styles.title}>Hello！欢迎加入这个购物网站项目！</h1>
      <div className={styles.welcome} />
      <ul className={styles.list}>
        <li>在进行项目开发之前，请你一定要阅读项目README文件</li>
      </ul>
      <Button type ="primary" onClick={() => alert("逗你玩！哈哈哈")}> Go </Button>
    </div>
  );
  }
}

IndexPage.propTypes = {
};

export default connect()(IndexPage);
