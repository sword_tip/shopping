# 包管理工具
 > 统一使用yarn，请先搭建yarn :https://yarnpkg.com/zh-Hans/

 如果已经装了node，可以直接执行以下命令安装yarn：
 ```
  npm i yarn -g
 ```

# 首次启动前准备
```
git clone git@gitee.com:Evan-90/shopping.git
cd shopping
yarn
```

# 启动
```
yarn start
```

# 打包构建
```
yarn build
```

# 分支管理
master：主干分支，保证稳定，只有开发完成并测试通过的功能才可以合并到master。master分支会被部署到生产环境。

dev：主要开发分支，用于日常开发，是开发中主要提交合并的分支。

#### 注意事项：
 未测试通过的代码请不要提交到master分支，开发以develop分支为主。
 ```
 # 为方便开发，你可以设置pull和push的默认分支为dev分支，命令如下：
 git branch --set-upstream-to=origin/dev dev
 ```

# git commit 规范
项目使用validate-commit-msg 用于检查 Commit message 是否符合规范,每次commit的时候会错误的提示要解决了才能提交上去。
##### 提交格式
```
<type>(<scope>): <subject>
```
type: commit的类型;
- feat：新功能（feature）
- fix：修补bug
- docs：文档（documentation）
- style： 格式（不影响代码运行的变动）
- refactor：重构（即不是新增功能，也不是修改bug的代码变动）
- test：增加测试
- chore：构建过程或辅助工具的变动

scope: 用于说明 commit 影响的范围，比如数据层、控制层、视图层等等，视项目不同而不同;
- 例如在React，可以是View, Router, Model等。
- 如果你的修改影响了不止一个scope，你可以使用*代替

subject:  commit 目的的简短描述，不超过50个字符;
- 以动词开头，使用第一人称现在时，比如change，而不是changed或changes
- 第一个字母小写
- 结尾不加句号（.）

# 项目结构
##### signal-react
- dist    —— 存放打包后的文件，是真正用于部署的成品
- mock    —— mock数据接口模拟，已有现成后台接口，可不使用
- node_modules  —— 安装的依赖包
- public  —— html模板
- src   ——   开发目录


```
根目录下其他主要文件说明
- .editorconfig  —— 统一编辑器排版风格
- .eslintrc   —— eslint代码语法检查
- .gitignore  —— 配置使用git来push代码时的忽略项
- .roadhogrc  —— roadhog打包工具的配置文件
- .roadhogrc.mock.js  —— mock文件
```

##### 开发目录——src
- assets      —— 静态资源目录，统一管理各模块的css、图片以及一些公共的js
- components  —— 存放展示组件，通常为模块下的子组件
- routes  —— 存放容器组件，通常为模块下的容器组件
- models      —— redux、异步请求等业务逻辑定义和处理
- services    —— 异步请求配置文件
- config      —— 路由等常用或公共配置文件
- utils       —— 工具库
- index.js    —— 入口文件
- router.js   —— 路由配置

# 开发规范
#### eslint检查
每次commit的时候都会执行代码检查，有错误的提示要解决了才能提交上去
> 直接commit检查比较慢的，可以分成两部走：先eslint检查，再提交。命令如下：
```
git add .
node check.js
//解决错误
git add .
git commit -m "XXXX" -n
```

#### css处理
1. 样式用css-modules和less编写，文件后缀统一用less。
2. 一个组件对应一个样式文件，样式文件跟组件放在一起。
3. 样式文件名和组件名保持一致，首字母大写。


### js
1. 请使用es6的语法。
2. 组件首字母都要大写。
3. 路由格式统一用小写+横杠。

后续再补充。。。

# 开发资料
### react相关资料
1. react中文文档：http://www.css88.com/react/index.html
2. dva说明文档：https://github.com/dvajs/dva/blob/master/docs/API_zh-CN.md
3. dva+es6开发文档： https://github.com/dvajs/dva-knowledgemap
4. redux中文文档：http://cn.redux.js.org/index.html
5. react-router4中文文档： http://618cj.com/react-router4-0%E8%B7%AF%E7%94%B1%E4%B8%AD%E6%96%87%E6%96%87%E6%A1%A3api/
6. ant design官方文档：https://ant.design/index-cn
7. es6中文文档：http://es6.ruanyifeng.com/
8. css-modules: https://github.com/css-modules/css-modules



更多待后续补充······
